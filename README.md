# BEJ

Binder with Extended Jupyter avec (ajouté à la volée, cf. apt.txt et requirements.txt, par ordre alphabétique) :
* ARM 
* Erlang 
* Prolog 
* R 
* ... (et d'autres langages \[ceux de base : C, Javascript, Python, ...])


et


* Rise (pour faire des présentations)
* NbGrader (pour évaluer du code)
* LaTeX, SpellChecker (pour produire des documents)


Lancement :
* Binder sur gricad (pour ceux qui sont sur le réseau uga, via éventuellement un vpn) : 
  * c'est ici (**choix conseillé**) : [![Binder](https://binderhub.univ-grenoble-alpes.fr/badge_logo.svg)](https://binderhub.univ-grenoble-alpes.fr/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdenisb%2Fbej/HEAD?labpath=index.ipynb)
* Sans Binder, sur votre ordi  (avec un jupyter et les pré-requis nécessaires, cf. apt.txt et requirements.txt)
  * (solution des petits débrouillard) si vous êtes prêt, récuperez les fichiers que vous voulez et débrouillez vous
* Avec myBynder (sinon, mais cet exemple est lourd, cela peut prendre du temps ; il y a d'autres exemples d'utilisation de binder plus légers): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdenisb%2Fbej/HEAD?labpath=index_.ipynb)


Mode d'emploi :
* [en vidéo ?](https://www.youtube.com/watch?v=n47b7vu0ucQ)

Rappel :
* pour installer/utiliser un vpn UGA : [nomadisme](https://nomadisme.grenet.fr/)


